package application;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

class LexiNodeTest {

	@Test
	void testAddMot() {
		LexiRacine racine = new LexiRacine();
		racine.addMot("allo", "salutation");
		racine.addMot("allo", "yoyoyo");
		System.out.println(racine.getNodeMotEnfant("allo").getDefinition());
		System.out.println(racine.getNodeMotEnfant("allo").getDefinition());
	}
	
	@Test
	void testEditMot() {
		LexiRacine racine = new LexiRacine();
		racine.addMot("GAULLISME", "subst. masc. Attitude politique de ceux qui ont �t� partisans du g�n�ral de Gaulle � l'�poque de la R�sistance et de la Lib�ration.");
		racine.addMot("GAULLISME", "sssque de ceux qui ont �t� partisans du g�n�ral de Gaulle � l'�poque de la R�sistance et de la Lib�ration.");
		Assert.assertEquals("subst. masc. Attitude politique de ceux qui ont �t� partisans du g�n�ral de Gaulle � l'�poque de la R�sistance et de la Lib�ration.", racine.getNodeMotEnfant("GAULLISME").getDefinition());
		racine.editMot("GAULLISME", "nouvelle def 123456789", "subst. masc. Attitude politique de ceux qui ont �t� partisans du g�n�ral de Gaulle � l'�poque de la R�sistance et de la Lib�ration.");
		Assert.assertEquals("nouvelle def 123456789", racine.getNodeMotEnfant("GAULLISME").getDefinition());
	}
	
	@Test
	void testGetAll() {
		LexiRacine racine = new LexiRacine();
		racine.addMot("allo", "salutation");
		racine.addMot("allo", "salwggation");
		racine.addMot("allf", "salutation");
		racine.addMot("allh", "saluwwaee");
		racine.addMot("alls", "salwwion");
		Assert.assertEquals(5, racine.getAllMots().size());
		Assert.assertEquals("allo", racine.getAllMots().get(0).getMotCourant());
		Assert.assertEquals("allo", racine.getAllMots().get(0).getMotCourant());
	}

}

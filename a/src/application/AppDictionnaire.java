package application;

import java.awt.EventQueue;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JTextField;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JTextPane;
import javax.swing.JScrollPane;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * Dictionnaire interactif dans lequel on peut ajouter et modifier des definitions pour des mots specifiques
 * @author Luis Angel Urena Lopez
 * @author Myriam Lacroix
 */
public class AppDictionnaire extends JFrame {

	final static long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField txtFieldSearch;
	private File file;
	private DefaultListModel<String> searchedWordsModel = new DefaultListModel<String>();
	private DefaultListModel<String> allWordsModel = new DefaultListModel<String>();
	private LinkedList<Integer> realIndexes = new LinkedList<Integer>();
	private JList<String> searchedWordsList;
	private LexiRacine racine = new LexiRacine();

	/**
	 * Methode main qui demarre l'application
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AppDictionnaire frame = new AppDictionnaire();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Creer la fenetre
	 */
	public AppDictionnaire() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 630, 259);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JButton btnNewButton = new JButton("Charger");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					file = choisirFichier();
					readTextFile(file);
					realIndexes = reloadSearchedWords();
				} catch(Exception e){
					System.out.println("Fichier introuvable");
				}
			}
		});
		btnNewButton.setBounds(182, 11, 111, 23);
		contentPane.add(btnNewButton);

		JButton btnNewButton_1 = new JButton("Enregistrer");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ecrireFichierData();
			}
		});
		btnNewButton_1.setBounds(339, 11, 111, 23);
		contentPane.add(btnNewButton_1);

		txtFieldSearch = new JTextField();
		txtFieldSearch.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				realIndexes = reloadSearchedWords();
			}
		});
		txtFieldSearch.setBounds(10, 45, 153, 20);
		contentPane.add(txtFieldSearch);
		txtFieldSearch.setColumns(10);

		File startingFile = new File("dictio.txt");
		readTextFile(startingFile);

		JTextPane txtPaneDefinitions = new JTextPane();
		txtPaneDefinitions.setBounds(173, 45, 277, 144);
		contentPane.add(txtPaneDefinitions);

		for(LexiNode node: racine.getAllMots()) {
			allWordsModel.addElement(node.getMotCourant());
		}

		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(10, 76, 153, 113);
		contentPane.add(scrollPane_1);

		realIndexes = reloadSearchedWords();


		searchedWordsList = new JList<String>(searchedWordsModel);
		searchedWordsList.setLocation(236, 0);
		searchedWordsList.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				if(searchedWordsList.getSelectedIndex() >= 0) 
					txtPaneDefinitions.setText(racine.getAllMots().get(realIndexes.get(searchedWordsList.getSelectedIndex())).getDefinition());
				else 
					txtPaneDefinitions.setText("");
			}
		});
		scrollPane_1.setViewportView(searchedWordsList);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(460, 45, 144, 144);
		contentPane.add(scrollPane);

		JList<String> allWordsList = new JList<String>(allWordsModel);
		allWordsList.setLocation(0, 45);
		scrollPane.setViewportView(allWordsList);

		JButton btnAjouterModifier = new JButton("Ajouter/Modifier");
		btnAjouterModifier.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
					String a = searchedWordsList.getSelectedValue();
					if(searchedWordsList.getSelectedValue() != null) {
						racine.editMot(searchedWordsList.getSelectedValue(), txtPaneDefinitions.getText(), racine.getAllMots().get(realIndexes.get(searchedWordsList.getSelectedIndex())).getDefinition());
					} else if (!txtFieldSearch.getText().equals("")) {
						racine.addMot(txtFieldSearch.getText(), txtPaneDefinitions.getText());
						realIndexes = reloadSearchedWords();
						allWordsModel.addElement(txtFieldSearch.getText());
					}
			}
		});
		btnAjouterModifier.setBounds(10, 192, 594, 23);
		contentPane.add(btnAjouterModifier);
	}

	/**
	 * Lire un fichier .txt contenant toutes les donnees personnalisees 
	 */
	private void readTextFile(File file) {
		BufferedReader fluxEntree = null;
		String entree=null;
		racine = new LexiRacine();


		try {
			fluxEntree = new BufferedReader(new FileReader(file));

			do { //r�p�ter tant que la fin de fichier n'est pas rencontr�e
				entree = fluxEntree.readLine();
				if (entree != null) {

					if(entree.indexOf('&') != -1) {
						String mot = entree.substring(0, entree.indexOf('&')-1);
						String definition = entree.substring(entree.indexOf('&')+2);
						racine.addMot(mot, definition);

					}
				}
			} while (entree != null);	

		} // fin try		

		catch (FileNotFoundException e) {
			JOptionPane.showMessageDialog(null, "Fichier  " + file.getAbsolutePath() + "  introuvable!");
			System.exit(0);
		}

		catch (IOException e) {
			JOptionPane.showMessageDialog(null, "Erreur rencontree lors de la lecture");
			e.printStackTrace();
			System.exit(0);
		}

		finally {
			//on ex�cutera toujours ceci, erreur ou pas
			try { 
				fluxEntree.close();  
			}
			catch (IOException e) { 
				System.out.println("Erreur rencontr�e lors de la fermeture!"); 
			}
		}//fin finally
	}

	/**
	 * Actualiser la liste des mots trouves a partir de la barre de recherche
	 * @return tableau avec les valeurs d'indices originaux pour chaque mot, utile pour obtenir leur definition meme apres avoir faite une recherche et que leurs indices changent
	 */
	private LinkedList<Integer> reloadSearchedWords() {
		LinkedList<Integer> realIndexes = new LinkedList<Integer>();
		int indexCounter = 0;
		searchedWordsModel.clear();	//on vide la liste pour re ajouter les bons mots
		for(LexiNode node : racine.getAllMots()) {		//on regarde pour chaque mot s'ils commencent par les lettres ecrites
			if(node.getMotCourant().toLowerCase().startsWith(txtFieldSearch.getText().toLowerCase())) {
				searchedWordsModel.addElement(node.getMotCourant());		//ajouter a la liste
				realIndexes.add(indexCounter);
			}
			indexCounter++;
		}
		return realIndexes;
	}

	/**
	 * Ecrire un fichier .txt contenant toutes les donnees personnalisees 
	 */
	//Used code from Nikolay Kuznetsov, in https://stackoverflow.com/questions/14479988/how-can-i-save-txt-file-at-desired-location-using-jfilechooser
	private void ecrireFichierData() {
		JFileChooser chooser = new JFileChooser();
		int returnVal = chooser.showSaveDialog(contentPane);
		
		if (returnVal == JFileChooser.APPROVE_OPTION) { //OK button pressed by user
			File fichierDeTravail = chooser.getSelectedFile(); //get File selected by user
			fichierDeTravail = chooser.getSelectedFile();

			PrintWriter fluxSortie=null;
			try {
				fluxSortie = new PrintWriter(new BufferedWriter(new FileWriter(fichierDeTravail + ".txt")));
				for(LexiNode node : racine.getAllMots()) {
					fluxSortie.println(node.getMotCourant() + " & " + node.getDefinition());
				}
				JOptionPane.showMessageDialog(null, "\nLe fichier " + fichierDeTravail.getAbsolutePath() + " a �t� sauvegard�.");
			} 
			catch (IOException e) {
				System.out.println("Erreur d'�criture!");
				e.printStackTrace();

			}
			finally {
				if (fluxSortie != null)
					fluxSortie.close();
			}
		}
	}

	/**
	 * Choisir un fichier dans l'ordinateur
	 */
	//Used code from Shashank Kadne, in https://stackoverflow.com/questions/9395367/making-a-dialog-box-that-allows-user-to-select-file
	private File choisirFichier() {
		JFileChooser jfc = new JFileChooser();
		jfc.showDialog(null,"Choisir le fichier .txt � lire");
		jfc.setVisible(true);
		return jfc.getSelectedFile();
	}
}

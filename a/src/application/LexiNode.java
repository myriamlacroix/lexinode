package application;
import java.util.*;

public class LexiNode {
	private char lettre;
	private String definition;
	private LexiNode parent;
	private LexiNode racine;
	private List<LexiNode> enfants;
	
	public char getLettre() {
		return lettre;
	}
	
	public String getDefinition() {
		return definition;
	}
	
	public LexiNode getParent() {
		return parent;
	}

	public LexiNode getRacine() {
		return racine;
	}
	
	public void setDefinition(String definition) {
		this.definition = definition;
	}
	
	public LexiNode() {
		this.racine = this;
		this.enfants = new ArrayList<LexiNode>();
	}
	
	public LexiNode(char lettre, String definition, LexiNode parent) {
		this.lettre = lettre;
		this.definition = definition;
		this.parent = parent;
		this.racine = parent.getRacine();
		this.enfants = new ArrayList<LexiNode>();
	}
	
	public LexiNode getEnfant(char lettre) {
		for (LexiNode n : enfants) {
			if (n.lettre == lettre) {
				return n;
			}
		}
		return null;
	}
	
	public LexiNode getEnfant(String definition) {
		for (LexiNode n : enfants) {
			if (n.definition == definition) {
				return n;
			}
		}
		return null;
	}
	
	/**
	 * Acc�der au mot courant, m�thode r�cursive
	 * @return Le mot en chaine de caract�res, retourne null si pas de mot courant
	 */
	public String getMotCourant() {
		if(definition == null) {
			return null;
		} else if(this.parent == null) {
			return Character.toString(lettre);
		}
		return parent.getMotParent();
	}
	
	/**
	 * Complexit� : O(n) o� n est le nombre de lettres dans le mot
	 * Acc�der au mot en remontant les lettres r�cursivement jusqu'au d�but
	 * @return Le mot en chaine de caract�res
	 */
	public String getMotParent() {
		if (this.parent == null) {
			return Character.toString(lettre);
		}
		return (parent.getMotParent() + Character.toString(lettre)).trim();
	}
	
	/**
	 * Complexit� : O(n) o� n est le nombre de lettres dans le mot
	 * Acc�der au mot en descendant les lettres r�cursivement jusqu'� la fin
	 * @return Le noeud final
	 */
	public LexiNode getNodeMotEnfant(String mot) {
		char lettre = mot.charAt(0);
		if (getEnfant(lettre) != null && mot.length() > 1) {
			return getEnfant(lettre).getNodeMotEnfant(mot.substring(1));
		} else if (mot.length() == 1 && getEnfant(lettre) != null) {
			return getEnfant(lettre).getEnfant(' ');
		}
		return null;
	}
	
	/**
	 * Complexit� : O(n) o� n est le nombre de lettres dans l'arbre
	 * Mets tous les mots complets (avec d�finition) dans une liste
	 * @return liste de tous les mots
	 */
	public LinkedList<LexiNode> getAllMots(){
		LinkedList<LexiNode> liste = new LinkedList<LexiNode>();
		for (LexiNode n : enfants) {
			if (n.getDefinition() != null) {
				liste.add(n);
			} else {
				liste.addAll(n.getAllMots());
			}
		}
		return liste;
	}
	
	/**
	 * Complexit� : O(n) o� n est le nombre de lettres du mot
	 * Ajoute un mot, lettre par lettre r�cursivement.
	 * @param mot : Lettres qui restent � ajouter
	 * @param definition : D�finition du mot final
	 */
	public void addMot(String mot, String definition) {
		if (mot.length() == 0) {
			enfants.add(new LexiNode(' ', definition, this));
			return;
		}
		char lettre = mot.charAt(0);
		if (getEnfant(lettre) != null) {
			getEnfant(lettre).addMot(mot.substring(1), definition);
		} else if (mot.length() >= 1){
			LexiNode n = new LexiNode(mot.charAt(0), null, this);
			if (mot.length() == 1) {
				n.enfants.add(new LexiNode(' ', definition, n));
			} else {
				n.addMot(mot.substring(1), definition);
			}
			enfants.add(n);
		}
	}
	
	/**
	 * Complexit� : O(n) o� n est le nombre de lettres du mot
	 * Modifie la d�finition du mot en passant par toutes les lettres r�cursivement
	 * @param mot : Lettres qui restent � parcourir
	 * @param nouvelleDefinition : D�finition � attribuer
	 * @param ancienneDefinition : Ancienne d�finition du mot
	 */
	public void editMot(String mot, String nouvelleDefinition, String ancienneDefinition) {
		char lettre = mot.charAt(0);
		if (getEnfant(lettre) != null && mot.length() > 1) {
			getEnfant(lettre).editMot(mot.substring(1), nouvelleDefinition, ancienneDefinition);
		} else if (mot.length() == 1) {
			getEnfant(lettre).getEnfant(ancienneDefinition).setDefinition(nouvelleDefinition);
		}
	}
}
